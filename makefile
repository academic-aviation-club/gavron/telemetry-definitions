DEFINITIONS_FILE=definitions.proto
PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"
OUT_DIR="./generated"

all: pre_build python typescript

typescript: generated/definitions_pb.js 
python: generated/definitions_pb2.py

node_modules: 
	npm install protobufjs typescript

venv:
	virtualenv venv
	. venv/bin/activate; pip install protobuf

pre_build:
	mkdir -p $(OUT_DIR)

generated/definitions_pb2.py: venv $(DEFINITIONS_FILE)
	protoc -I . $(DEFINITIONS_FILE) --python_out=$(OUT_DIR)

generated/definitions_pb.js: node_modules $(DEFINITIONS_FILE)
	node ./node_modules/protobufjs/cli/bin/pbjs \
	-t static-module -w commonjs -o generated/GavronTelem.js \
	./definitions.proto

	node ./node_modules/protobufjs/cli/bin/pbts \
	-o generated/GavronTelem.js.d.ts generated/GavronTelem.js 
	
	cp generated/GavronTelem.js.d.ts generated/GavronTelem.d.ts
test: all
	./venv/bin/python test.py;
	rm -rf test.js
	tsc test.ts;
	bash test.sh
	@echo
	@echo All good
	
clear:
	rm -rf $(OUT_DIR) venv node_modules package-lock.json test.js msg.bin
