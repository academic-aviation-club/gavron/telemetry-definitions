from generated import definitions_pb2

if __name__ == "__main__":
    frame = definitions_pb2.GavronTelemFrame()

    frame.header.timestamp = 123123
    frame.header.machine_id = 1
    frame.header.flight_id = 5

    frame.header.position.lat = 5
    frame.header.position.lng = 10
    frame.header.position.alt = 15
    frame.header.position.heading = 20
    
    print("Serialised frame:")
    print(frame.SerializeToString())

    with open('/tmp/msg.bin', 'wb') as message_save_file:
        message_save_file.write(frame.SerializeToString())
