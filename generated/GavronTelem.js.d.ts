import * as $protobuf from "protobufjs";
/** Namespace GavronTelem. */
export namespace GavronTelem {

    /** Properties of a Position. */
    interface IPosition {

        /** Position lat */
        lat?: (number|null);

        /** Position lng */
        lng?: (number|null);

        /** Position alt */
        alt?: (number|null);

        /** Position heading */
        heading?: (number|null);
    }

    /** Represents a Position. */
    class Position implements IPosition {

        /**
         * Constructs a new Position.
         * @param [properties] Properties to set
         */
        constructor(properties?: GavronTelem.IPosition);

        /** Position lat. */
        public lat: number;

        /** Position lng. */
        public lng: number;

        /** Position alt. */
        public alt: number;

        /** Position heading. */
        public heading: number;

        /**
         * Creates a new Position instance using the specified properties.
         * @param [properties] Properties to set
         * @returns Position instance
         */
        public static create(properties?: GavronTelem.IPosition): GavronTelem.Position;

        /**
         * Encodes the specified Position message. Does not implicitly {@link GavronTelem.Position.verify|verify} messages.
         * @param message Position message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: GavronTelem.IPosition, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified Position message, length delimited. Does not implicitly {@link GavronTelem.Position.verify|verify} messages.
         * @param message Position message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: GavronTelem.IPosition, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a Position message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns Position
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): GavronTelem.Position;

        /**
         * Decodes a Position message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns Position
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): GavronTelem.Position;

        /**
         * Verifies a Position message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a Position message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns Position
         */
        public static fromObject(object: { [k: string]: any }): GavronTelem.Position;

        /**
         * Creates a plain object from a Position message. Also converts values to other types if specified.
         * @param message Position
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: GavronTelem.Position, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this Position to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GavronTelemFrameHeader. */
    interface IGavronTelemFrameHeader {

        /** GavronTelemFrameHeader timestamp */
        timestamp?: (number|null);

        /** GavronTelemFrameHeader machineId */
        machineId?: (number|null);

        /** GavronTelemFrameHeader flightId */
        flightId?: (number|null);

        /** GavronTelemFrameHeader position */
        position?: (GavronTelem.IPosition|null);
    }

    /** Represents a GavronTelemFrameHeader. */
    class GavronTelemFrameHeader implements IGavronTelemFrameHeader {

        /**
         * Constructs a new GavronTelemFrameHeader.
         * @param [properties] Properties to set
         */
        constructor(properties?: GavronTelem.IGavronTelemFrameHeader);

        /** GavronTelemFrameHeader timestamp. */
        public timestamp: number;

        /** GavronTelemFrameHeader machineId. */
        public machineId: number;

        /** GavronTelemFrameHeader flightId. */
        public flightId: number;

        /** GavronTelemFrameHeader position. */
        public position?: (GavronTelem.IPosition|null);

        /**
         * Creates a new GavronTelemFrameHeader instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GavronTelemFrameHeader instance
         */
        public static create(properties?: GavronTelem.IGavronTelemFrameHeader): GavronTelem.GavronTelemFrameHeader;

        /**
         * Encodes the specified GavronTelemFrameHeader message. Does not implicitly {@link GavronTelem.GavronTelemFrameHeader.verify|verify} messages.
         * @param message GavronTelemFrameHeader message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: GavronTelem.IGavronTelemFrameHeader, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GavronTelemFrameHeader message, length delimited. Does not implicitly {@link GavronTelem.GavronTelemFrameHeader.verify|verify} messages.
         * @param message GavronTelemFrameHeader message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: GavronTelem.IGavronTelemFrameHeader, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GavronTelemFrameHeader message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GavronTelemFrameHeader
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): GavronTelem.GavronTelemFrameHeader;

        /**
         * Decodes a GavronTelemFrameHeader message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GavronTelemFrameHeader
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): GavronTelem.GavronTelemFrameHeader;

        /**
         * Verifies a GavronTelemFrameHeader message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GavronTelemFrameHeader message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GavronTelemFrameHeader
         */
        public static fromObject(object: { [k: string]: any }): GavronTelem.GavronTelemFrameHeader;

        /**
         * Creates a plain object from a GavronTelemFrameHeader message. Also converts values to other types if specified.
         * @param message GavronTelemFrameHeader
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: GavronTelem.GavronTelemFrameHeader, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GavronTelemFrameHeader to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a DroneMonitoringMessage. */
    interface IDroneMonitoringMessage {

        /** DroneMonitoringMessage cameraOn */
        cameraOn?: (boolean|null);

        /** DroneMonitoringMessage batteryLeft */
        batteryLeft?: (number|null);
    }

    /** Represents a DroneMonitoringMessage. */
    class DroneMonitoringMessage implements IDroneMonitoringMessage {

        /**
         * Constructs a new DroneMonitoringMessage.
         * @param [properties] Properties to set
         */
        constructor(properties?: GavronTelem.IDroneMonitoringMessage);

        /** DroneMonitoringMessage cameraOn. */
        public cameraOn: boolean;

        /** DroneMonitoringMessage batteryLeft. */
        public batteryLeft: number;

        /**
         * Creates a new DroneMonitoringMessage instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DroneMonitoringMessage instance
         */
        public static create(properties?: GavronTelem.IDroneMonitoringMessage): GavronTelem.DroneMonitoringMessage;

        /**
         * Encodes the specified DroneMonitoringMessage message. Does not implicitly {@link GavronTelem.DroneMonitoringMessage.verify|verify} messages.
         * @param message DroneMonitoringMessage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: GavronTelem.IDroneMonitoringMessage, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified DroneMonitoringMessage message, length delimited. Does not implicitly {@link GavronTelem.DroneMonitoringMessage.verify|verify} messages.
         * @param message DroneMonitoringMessage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: GavronTelem.IDroneMonitoringMessage, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a DroneMonitoringMessage message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DroneMonitoringMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): GavronTelem.DroneMonitoringMessage;

        /**
         * Decodes a DroneMonitoringMessage message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DroneMonitoringMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): GavronTelem.DroneMonitoringMessage;

        /**
         * Verifies a DroneMonitoringMessage message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a DroneMonitoringMessage message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns DroneMonitoringMessage
         */
        public static fromObject(object: { [k: string]: any }): GavronTelem.DroneMonitoringMessage;

        /**
         * Creates a plain object from a DroneMonitoringMessage message. Also converts values to other types if specified.
         * @param message DroneMonitoringMessage
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: GavronTelem.DroneMonitoringMessage, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this DroneMonitoringMessage to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SaeDropDataMessage. */
    interface ISaeDropDataMessage {

        /** SaeDropDataMessage cargoDropManual */
        cargoDropManual?: (boolean|null);

        /** SaeDropDataMessage cargoDropAuto */
        cargoDropAuto?: (boolean|null);

        /** SaeDropDataMessage glidersDropManual */
        glidersDropManual?: (boolean|null);

        /** SaeDropDataMessage glidersDropAuto */
        glidersDropAuto?: (boolean|null);

        /** SaeDropDataMessage targetLat */
        targetLat?: (number|null);

        /** SaeDropDataMessage targetLng */
        targetLng?: (number|null);

        /** SaeDropDataMessage predictedDropLat */
        predictedDropLat?: (number|null);

        /** SaeDropDataMessage predictedDropLng */
        predictedDropLng?: (number|null);

        /** SaeDropDataMessage distanceToTarget */
        distanceToTarget?: (number|null);

        /** SaeDropDataMessage cargoDropped */
        cargoDropped?: (boolean|null);

        /** SaeDropDataMessage glidersDropped */
        glidersDropped?: (boolean|null);
    }

    /** Represents a SaeDropDataMessage. */
    class SaeDropDataMessage implements ISaeDropDataMessage {

        /**
         * Constructs a new SaeDropDataMessage.
         * @param [properties] Properties to set
         */
        constructor(properties?: GavronTelem.ISaeDropDataMessage);

        /** SaeDropDataMessage cargoDropManual. */
        public cargoDropManual: boolean;

        /** SaeDropDataMessage cargoDropAuto. */
        public cargoDropAuto: boolean;

        /** SaeDropDataMessage glidersDropManual. */
        public glidersDropManual: boolean;

        /** SaeDropDataMessage glidersDropAuto. */
        public glidersDropAuto: boolean;

        /** SaeDropDataMessage targetLat. */
        public targetLat: number;

        /** SaeDropDataMessage targetLng. */
        public targetLng: number;

        /** SaeDropDataMessage predictedDropLat. */
        public predictedDropLat: number;

        /** SaeDropDataMessage predictedDropLng. */
        public predictedDropLng: number;

        /** SaeDropDataMessage distanceToTarget. */
        public distanceToTarget: number;

        /** SaeDropDataMessage cargoDropped. */
        public cargoDropped: boolean;

        /** SaeDropDataMessage glidersDropped. */
        public glidersDropped: boolean;

        /**
         * Creates a new SaeDropDataMessage instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SaeDropDataMessage instance
         */
        public static create(properties?: GavronTelem.ISaeDropDataMessage): GavronTelem.SaeDropDataMessage;

        /**
         * Encodes the specified SaeDropDataMessage message. Does not implicitly {@link GavronTelem.SaeDropDataMessage.verify|verify} messages.
         * @param message SaeDropDataMessage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: GavronTelem.ISaeDropDataMessage, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SaeDropDataMessage message, length delimited. Does not implicitly {@link GavronTelem.SaeDropDataMessage.verify|verify} messages.
         * @param message SaeDropDataMessage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: GavronTelem.ISaeDropDataMessage, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SaeDropDataMessage message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SaeDropDataMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): GavronTelem.SaeDropDataMessage;

        /**
         * Decodes a SaeDropDataMessage message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SaeDropDataMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): GavronTelem.SaeDropDataMessage;

        /**
         * Verifies a SaeDropDataMessage message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SaeDropDataMessage message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SaeDropDataMessage
         */
        public static fromObject(object: { [k: string]: any }): GavronTelem.SaeDropDataMessage;

        /**
         * Creates a plain object from a SaeDropDataMessage message. Also converts values to other types if specified.
         * @param message SaeDropDataMessage
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: GavronTelem.SaeDropDataMessage, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SaeDropDataMessage to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a DroniadaObjectDetectionMessage. */
    interface IDroniadaObjectDetectionMessage {

        /** DroniadaObjectDetectionMessage objectLat */
        objectLat?: (number|null);

        /** DroniadaObjectDetectionMessage objectLng */
        objectLng?: (number|null);

        /** DroniadaObjectDetectionMessage objectX */
        objectX?: (number|null);

        /** DroniadaObjectDetectionMessage objectY */
        objectY?: (number|null);

        /** DroniadaObjectDetectionMessage objectId */
        objectId?: (number|null);

        /** DroniadaObjectDetectionMessage objectClass */
        objectClass?: (number|null);
    }

    /** Represents a DroniadaObjectDetectionMessage. */
    class DroniadaObjectDetectionMessage implements IDroniadaObjectDetectionMessage {

        /**
         * Constructs a new DroniadaObjectDetectionMessage.
         * @param [properties] Properties to set
         */
        constructor(properties?: GavronTelem.IDroniadaObjectDetectionMessage);

        /** DroniadaObjectDetectionMessage objectLat. */
        public objectLat: number;

        /** DroniadaObjectDetectionMessage objectLng. */
        public objectLng: number;

        /** DroniadaObjectDetectionMessage objectX. */
        public objectX: number;

        /** DroniadaObjectDetectionMessage objectY. */
        public objectY: number;

        /** DroniadaObjectDetectionMessage objectId. */
        public objectId: number;

        /** DroniadaObjectDetectionMessage objectClass. */
        public objectClass: number;

        /**
         * Creates a new DroniadaObjectDetectionMessage instance using the specified properties.
         * @param [properties] Properties to set
         * @returns DroniadaObjectDetectionMessage instance
         */
        public static create(properties?: GavronTelem.IDroniadaObjectDetectionMessage): GavronTelem.DroniadaObjectDetectionMessage;

        /**
         * Encodes the specified DroniadaObjectDetectionMessage message. Does not implicitly {@link GavronTelem.DroniadaObjectDetectionMessage.verify|verify} messages.
         * @param message DroniadaObjectDetectionMessage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: GavronTelem.IDroniadaObjectDetectionMessage, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified DroniadaObjectDetectionMessage message, length delimited. Does not implicitly {@link GavronTelem.DroniadaObjectDetectionMessage.verify|verify} messages.
         * @param message DroniadaObjectDetectionMessage message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: GavronTelem.IDroniadaObjectDetectionMessage, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a DroniadaObjectDetectionMessage message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns DroniadaObjectDetectionMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): GavronTelem.DroniadaObjectDetectionMessage;

        /**
         * Decodes a DroniadaObjectDetectionMessage message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns DroniadaObjectDetectionMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): GavronTelem.DroniadaObjectDetectionMessage;

        /**
         * Verifies a DroniadaObjectDetectionMessage message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a DroniadaObjectDetectionMessage message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns DroniadaObjectDetectionMessage
         */
        public static fromObject(object: { [k: string]: any }): GavronTelem.DroniadaObjectDetectionMessage;

        /**
         * Creates a plain object from a DroniadaObjectDetectionMessage message. Also converts values to other types if specified.
         * @param message DroniadaObjectDetectionMessage
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: GavronTelem.DroniadaObjectDetectionMessage, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this DroniadaObjectDetectionMessage to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a GavronTelemFrame. */
    interface IGavronTelemFrame {

        /** GavronTelemFrame header */
        header?: (GavronTelem.IGavronTelemFrameHeader|null);

        /** GavronTelemFrame droneMonitoringMessage */
        droneMonitoringMessage?: (GavronTelem.IDroneMonitoringMessage|null);

        /** GavronTelemFrame saeDropDataMessage */
        saeDropDataMessage?: (GavronTelem.ISaeDropDataMessage|null);

        /** GavronTelemFrame droniadaObjectDetectionMessage */
        droniadaObjectDetectionMessage?: (GavronTelem.IDroniadaObjectDetectionMessage|null);
    }

    /** Represents a GavronTelemFrame. */
    class GavronTelemFrame implements IGavronTelemFrame {

        /**
         * Constructs a new GavronTelemFrame.
         * @param [properties] Properties to set
         */
        constructor(properties?: GavronTelem.IGavronTelemFrame);

        /** GavronTelemFrame header. */
        public header?: (GavronTelem.IGavronTelemFrameHeader|null);

        /** GavronTelemFrame droneMonitoringMessage. */
        public droneMonitoringMessage?: (GavronTelem.IDroneMonitoringMessage|null);

        /** GavronTelemFrame saeDropDataMessage. */
        public saeDropDataMessage?: (GavronTelem.ISaeDropDataMessage|null);

        /** GavronTelemFrame droniadaObjectDetectionMessage. */
        public droniadaObjectDetectionMessage?: (GavronTelem.IDroniadaObjectDetectionMessage|null);

        /** GavronTelemFrame messages. */
        public messages?: ("droneMonitoringMessage"|"saeDropDataMessage"|"droniadaObjectDetectionMessage");

        /**
         * Creates a new GavronTelemFrame instance using the specified properties.
         * @param [properties] Properties to set
         * @returns GavronTelemFrame instance
         */
        public static create(properties?: GavronTelem.IGavronTelemFrame): GavronTelem.GavronTelemFrame;

        /**
         * Encodes the specified GavronTelemFrame message. Does not implicitly {@link GavronTelem.GavronTelemFrame.verify|verify} messages.
         * @param message GavronTelemFrame message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: GavronTelem.IGavronTelemFrame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified GavronTelemFrame message, length delimited. Does not implicitly {@link GavronTelem.GavronTelemFrame.verify|verify} messages.
         * @param message GavronTelemFrame message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: GavronTelem.IGavronTelemFrame, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a GavronTelemFrame message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns GavronTelemFrame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): GavronTelem.GavronTelemFrame;

        /**
         * Decodes a GavronTelemFrame message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns GavronTelemFrame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): GavronTelem.GavronTelemFrame;

        /**
         * Verifies a GavronTelemFrame message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a GavronTelemFrame message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns GavronTelemFrame
         */
        public static fromObject(object: { [k: string]: any }): GavronTelem.GavronTelemFrame;

        /**
         * Creates a plain object from a GavronTelemFrame message. Also converts values to other types if specified.
         * @param message GavronTelemFrame
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: GavronTelem.GavronTelemFrame, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this GavronTelemFrame to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}
