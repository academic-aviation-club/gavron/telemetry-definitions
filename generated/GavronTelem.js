/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.GavronTelem = (function() {

    /**
     * Namespace GavronTelem.
     * @exports GavronTelem
     * @namespace
     */
    var GavronTelem = {};

    GavronTelem.Position = (function() {

        /**
         * Properties of a Position.
         * @memberof GavronTelem
         * @interface IPosition
         * @property {number|null} [lat] Position lat
         * @property {number|null} [lng] Position lng
         * @property {number|null} [alt] Position alt
         * @property {number|null} [heading] Position heading
         */

        /**
         * Constructs a new Position.
         * @memberof GavronTelem
         * @classdesc Represents a Position.
         * @implements IPosition
         * @constructor
         * @param {GavronTelem.IPosition=} [properties] Properties to set
         */
        function Position(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * Position lat.
         * @member {number} lat
         * @memberof GavronTelem.Position
         * @instance
         */
        Position.prototype.lat = 0;

        /**
         * Position lng.
         * @member {number} lng
         * @memberof GavronTelem.Position
         * @instance
         */
        Position.prototype.lng = 0;

        /**
         * Position alt.
         * @member {number} alt
         * @memberof GavronTelem.Position
         * @instance
         */
        Position.prototype.alt = 0;

        /**
         * Position heading.
         * @member {number} heading
         * @memberof GavronTelem.Position
         * @instance
         */
        Position.prototype.heading = 0;

        /**
         * Creates a new Position instance using the specified properties.
         * @function create
         * @memberof GavronTelem.Position
         * @static
         * @param {GavronTelem.IPosition=} [properties] Properties to set
         * @returns {GavronTelem.Position} Position instance
         */
        Position.create = function create(properties) {
            return new Position(properties);
        };

        /**
         * Encodes the specified Position message. Does not implicitly {@link GavronTelem.Position.verify|verify} messages.
         * @function encode
         * @memberof GavronTelem.Position
         * @static
         * @param {GavronTelem.IPosition} message Position message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Position.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.lat != null && Object.hasOwnProperty.call(message, "lat"))
                writer.uint32(/* id 1, wireType 5 =*/13).float(message.lat);
            if (message.lng != null && Object.hasOwnProperty.call(message, "lng"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.lng);
            if (message.alt != null && Object.hasOwnProperty.call(message, "alt"))
                writer.uint32(/* id 3, wireType 5 =*/29).float(message.alt);
            if (message.heading != null && Object.hasOwnProperty.call(message, "heading"))
                writer.uint32(/* id 4, wireType 5 =*/37).float(message.heading);
            return writer;
        };

        /**
         * Encodes the specified Position message, length delimited. Does not implicitly {@link GavronTelem.Position.verify|verify} messages.
         * @function encodeDelimited
         * @memberof GavronTelem.Position
         * @static
         * @param {GavronTelem.IPosition} message Position message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        Position.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a Position message from the specified reader or buffer.
         * @function decode
         * @memberof GavronTelem.Position
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {GavronTelem.Position} Position
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Position.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GavronTelem.Position();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.lat = reader.float();
                    break;
                case 2:
                    message.lng = reader.float();
                    break;
                case 3:
                    message.alt = reader.float();
                    break;
                case 4:
                    message.heading = reader.float();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a Position message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof GavronTelem.Position
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {GavronTelem.Position} Position
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        Position.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a Position message.
         * @function verify
         * @memberof GavronTelem.Position
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        Position.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.lat != null && message.hasOwnProperty("lat"))
                if (typeof message.lat !== "number")
                    return "lat: number expected";
            if (message.lng != null && message.hasOwnProperty("lng"))
                if (typeof message.lng !== "number")
                    return "lng: number expected";
            if (message.alt != null && message.hasOwnProperty("alt"))
                if (typeof message.alt !== "number")
                    return "alt: number expected";
            if (message.heading != null && message.hasOwnProperty("heading"))
                if (typeof message.heading !== "number")
                    return "heading: number expected";
            return null;
        };

        /**
         * Creates a Position message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof GavronTelem.Position
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {GavronTelem.Position} Position
         */
        Position.fromObject = function fromObject(object) {
            if (object instanceof $root.GavronTelem.Position)
                return object;
            var message = new $root.GavronTelem.Position();
            if (object.lat != null)
                message.lat = Number(object.lat);
            if (object.lng != null)
                message.lng = Number(object.lng);
            if (object.alt != null)
                message.alt = Number(object.alt);
            if (object.heading != null)
                message.heading = Number(object.heading);
            return message;
        };

        /**
         * Creates a plain object from a Position message. Also converts values to other types if specified.
         * @function toObject
         * @memberof GavronTelem.Position
         * @static
         * @param {GavronTelem.Position} message Position
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        Position.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.lat = 0;
                object.lng = 0;
                object.alt = 0;
                object.heading = 0;
            }
            if (message.lat != null && message.hasOwnProperty("lat"))
                object.lat = options.json && !isFinite(message.lat) ? String(message.lat) : message.lat;
            if (message.lng != null && message.hasOwnProperty("lng"))
                object.lng = options.json && !isFinite(message.lng) ? String(message.lng) : message.lng;
            if (message.alt != null && message.hasOwnProperty("alt"))
                object.alt = options.json && !isFinite(message.alt) ? String(message.alt) : message.alt;
            if (message.heading != null && message.hasOwnProperty("heading"))
                object.heading = options.json && !isFinite(message.heading) ? String(message.heading) : message.heading;
            return object;
        };

        /**
         * Converts this Position to JSON.
         * @function toJSON
         * @memberof GavronTelem.Position
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        Position.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return Position;
    })();

    GavronTelem.GavronTelemFrameHeader = (function() {

        /**
         * Properties of a GavronTelemFrameHeader.
         * @memberof GavronTelem
         * @interface IGavronTelemFrameHeader
         * @property {number|null} [timestamp] GavronTelemFrameHeader timestamp
         * @property {number|null} [machineId] GavronTelemFrameHeader machineId
         * @property {number|null} [flightId] GavronTelemFrameHeader flightId
         * @property {GavronTelem.IPosition|null} [position] GavronTelemFrameHeader position
         */

        /**
         * Constructs a new GavronTelemFrameHeader.
         * @memberof GavronTelem
         * @classdesc Represents a GavronTelemFrameHeader.
         * @implements IGavronTelemFrameHeader
         * @constructor
         * @param {GavronTelem.IGavronTelemFrameHeader=} [properties] Properties to set
         */
        function GavronTelemFrameHeader(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GavronTelemFrameHeader timestamp.
         * @member {number} timestamp
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @instance
         */
        GavronTelemFrameHeader.prototype.timestamp = 0;

        /**
         * GavronTelemFrameHeader machineId.
         * @member {number} machineId
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @instance
         */
        GavronTelemFrameHeader.prototype.machineId = 0;

        /**
         * GavronTelemFrameHeader flightId.
         * @member {number} flightId
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @instance
         */
        GavronTelemFrameHeader.prototype.flightId = 0;

        /**
         * GavronTelemFrameHeader position.
         * @member {GavronTelem.IPosition|null|undefined} position
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @instance
         */
        GavronTelemFrameHeader.prototype.position = null;

        /**
         * Creates a new GavronTelemFrameHeader instance using the specified properties.
         * @function create
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {GavronTelem.IGavronTelemFrameHeader=} [properties] Properties to set
         * @returns {GavronTelem.GavronTelemFrameHeader} GavronTelemFrameHeader instance
         */
        GavronTelemFrameHeader.create = function create(properties) {
            return new GavronTelemFrameHeader(properties);
        };

        /**
         * Encodes the specified GavronTelemFrameHeader message. Does not implicitly {@link GavronTelem.GavronTelemFrameHeader.verify|verify} messages.
         * @function encode
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {GavronTelem.IGavronTelemFrameHeader} message GavronTelemFrameHeader message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GavronTelemFrameHeader.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.timestamp != null && Object.hasOwnProperty.call(message, "timestamp"))
                writer.uint32(/* id 1, wireType 5 =*/13).fixed32(message.timestamp);
            if (message.machineId != null && Object.hasOwnProperty.call(message, "machineId"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.machineId);
            if (message.flightId != null && Object.hasOwnProperty.call(message, "flightId"))
                writer.uint32(/* id 3, wireType 5 =*/29).fixed32(message.flightId);
            if (message.position != null && Object.hasOwnProperty.call(message, "position"))
                $root.GavronTelem.Position.encode(message.position, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified GavronTelemFrameHeader message, length delimited. Does not implicitly {@link GavronTelem.GavronTelemFrameHeader.verify|verify} messages.
         * @function encodeDelimited
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {GavronTelem.IGavronTelemFrameHeader} message GavronTelemFrameHeader message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GavronTelemFrameHeader.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GavronTelemFrameHeader message from the specified reader or buffer.
         * @function decode
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {GavronTelem.GavronTelemFrameHeader} GavronTelemFrameHeader
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GavronTelemFrameHeader.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GavronTelem.GavronTelemFrameHeader();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.timestamp = reader.fixed32();
                    break;
                case 2:
                    message.machineId = reader.int32();
                    break;
                case 3:
                    message.flightId = reader.fixed32();
                    break;
                case 4:
                    message.position = $root.GavronTelem.Position.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GavronTelemFrameHeader message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {GavronTelem.GavronTelemFrameHeader} GavronTelemFrameHeader
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GavronTelemFrameHeader.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GavronTelemFrameHeader message.
         * @function verify
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GavronTelemFrameHeader.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                if (!$util.isInteger(message.timestamp))
                    return "timestamp: integer expected";
            if (message.machineId != null && message.hasOwnProperty("machineId"))
                if (!$util.isInteger(message.machineId))
                    return "machineId: integer expected";
            if (message.flightId != null && message.hasOwnProperty("flightId"))
                if (!$util.isInteger(message.flightId))
                    return "flightId: integer expected";
            if (message.position != null && message.hasOwnProperty("position")) {
                var error = $root.GavronTelem.Position.verify(message.position);
                if (error)
                    return "position." + error;
            }
            return null;
        };

        /**
         * Creates a GavronTelemFrameHeader message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {GavronTelem.GavronTelemFrameHeader} GavronTelemFrameHeader
         */
        GavronTelemFrameHeader.fromObject = function fromObject(object) {
            if (object instanceof $root.GavronTelem.GavronTelemFrameHeader)
                return object;
            var message = new $root.GavronTelem.GavronTelemFrameHeader();
            if (object.timestamp != null)
                message.timestamp = object.timestamp >>> 0;
            if (object.machineId != null)
                message.machineId = object.machineId | 0;
            if (object.flightId != null)
                message.flightId = object.flightId >>> 0;
            if (object.position != null) {
                if (typeof object.position !== "object")
                    throw TypeError(".GavronTelem.GavronTelemFrameHeader.position: object expected");
                message.position = $root.GavronTelem.Position.fromObject(object.position);
            }
            return message;
        };

        /**
         * Creates a plain object from a GavronTelemFrameHeader message. Also converts values to other types if specified.
         * @function toObject
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @static
         * @param {GavronTelem.GavronTelemFrameHeader} message GavronTelemFrameHeader
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GavronTelemFrameHeader.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.timestamp = 0;
                object.machineId = 0;
                object.flightId = 0;
                object.position = null;
            }
            if (message.timestamp != null && message.hasOwnProperty("timestamp"))
                object.timestamp = message.timestamp;
            if (message.machineId != null && message.hasOwnProperty("machineId"))
                object.machineId = message.machineId;
            if (message.flightId != null && message.hasOwnProperty("flightId"))
                object.flightId = message.flightId;
            if (message.position != null && message.hasOwnProperty("position"))
                object.position = $root.GavronTelem.Position.toObject(message.position, options);
            return object;
        };

        /**
         * Converts this GavronTelemFrameHeader to JSON.
         * @function toJSON
         * @memberof GavronTelem.GavronTelemFrameHeader
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GavronTelemFrameHeader.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return GavronTelemFrameHeader;
    })();

    GavronTelem.DroneMonitoringMessage = (function() {

        /**
         * Properties of a DroneMonitoringMessage.
         * @memberof GavronTelem
         * @interface IDroneMonitoringMessage
         * @property {boolean|null} [cameraOn] DroneMonitoringMessage cameraOn
         * @property {number|null} [batteryLeft] DroneMonitoringMessage batteryLeft
         */

        /**
         * Constructs a new DroneMonitoringMessage.
         * @memberof GavronTelem
         * @classdesc Represents a DroneMonitoringMessage.
         * @implements IDroneMonitoringMessage
         * @constructor
         * @param {GavronTelem.IDroneMonitoringMessage=} [properties] Properties to set
         */
        function DroneMonitoringMessage(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * DroneMonitoringMessage cameraOn.
         * @member {boolean} cameraOn
         * @memberof GavronTelem.DroneMonitoringMessage
         * @instance
         */
        DroneMonitoringMessage.prototype.cameraOn = false;

        /**
         * DroneMonitoringMessage batteryLeft.
         * @member {number} batteryLeft
         * @memberof GavronTelem.DroneMonitoringMessage
         * @instance
         */
        DroneMonitoringMessage.prototype.batteryLeft = 0;

        /**
         * Creates a new DroneMonitoringMessage instance using the specified properties.
         * @function create
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {GavronTelem.IDroneMonitoringMessage=} [properties] Properties to set
         * @returns {GavronTelem.DroneMonitoringMessage} DroneMonitoringMessage instance
         */
        DroneMonitoringMessage.create = function create(properties) {
            return new DroneMonitoringMessage(properties);
        };

        /**
         * Encodes the specified DroneMonitoringMessage message. Does not implicitly {@link GavronTelem.DroneMonitoringMessage.verify|verify} messages.
         * @function encode
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {GavronTelem.IDroneMonitoringMessage} message DroneMonitoringMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DroneMonitoringMessage.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.cameraOn != null && Object.hasOwnProperty.call(message, "cameraOn"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.cameraOn);
            if (message.batteryLeft != null && Object.hasOwnProperty.call(message, "batteryLeft"))
                writer.uint32(/* id 2, wireType 0 =*/16).int32(message.batteryLeft);
            return writer;
        };

        /**
         * Encodes the specified DroneMonitoringMessage message, length delimited. Does not implicitly {@link GavronTelem.DroneMonitoringMessage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {GavronTelem.IDroneMonitoringMessage} message DroneMonitoringMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DroneMonitoringMessage.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DroneMonitoringMessage message from the specified reader or buffer.
         * @function decode
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {GavronTelem.DroneMonitoringMessage} DroneMonitoringMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DroneMonitoringMessage.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GavronTelem.DroneMonitoringMessage();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.cameraOn = reader.bool();
                    break;
                case 2:
                    message.batteryLeft = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DroneMonitoringMessage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {GavronTelem.DroneMonitoringMessage} DroneMonitoringMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DroneMonitoringMessage.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DroneMonitoringMessage message.
         * @function verify
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        DroneMonitoringMessage.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.cameraOn != null && message.hasOwnProperty("cameraOn"))
                if (typeof message.cameraOn !== "boolean")
                    return "cameraOn: boolean expected";
            if (message.batteryLeft != null && message.hasOwnProperty("batteryLeft"))
                if (!$util.isInteger(message.batteryLeft))
                    return "batteryLeft: integer expected";
            return null;
        };

        /**
         * Creates a DroneMonitoringMessage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {GavronTelem.DroneMonitoringMessage} DroneMonitoringMessage
         */
        DroneMonitoringMessage.fromObject = function fromObject(object) {
            if (object instanceof $root.GavronTelem.DroneMonitoringMessage)
                return object;
            var message = new $root.GavronTelem.DroneMonitoringMessage();
            if (object.cameraOn != null)
                message.cameraOn = Boolean(object.cameraOn);
            if (object.batteryLeft != null)
                message.batteryLeft = object.batteryLeft | 0;
            return message;
        };

        /**
         * Creates a plain object from a DroneMonitoringMessage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof GavronTelem.DroneMonitoringMessage
         * @static
         * @param {GavronTelem.DroneMonitoringMessage} message DroneMonitoringMessage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DroneMonitoringMessage.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.cameraOn = false;
                object.batteryLeft = 0;
            }
            if (message.cameraOn != null && message.hasOwnProperty("cameraOn"))
                object.cameraOn = message.cameraOn;
            if (message.batteryLeft != null && message.hasOwnProperty("batteryLeft"))
                object.batteryLeft = message.batteryLeft;
            return object;
        };

        /**
         * Converts this DroneMonitoringMessage to JSON.
         * @function toJSON
         * @memberof GavronTelem.DroneMonitoringMessage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        DroneMonitoringMessage.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DroneMonitoringMessage;
    })();

    GavronTelem.SaeDropDataMessage = (function() {

        /**
         * Properties of a SaeDropDataMessage.
         * @memberof GavronTelem
         * @interface ISaeDropDataMessage
         * @property {boolean|null} [cargoDropManual] SaeDropDataMessage cargoDropManual
         * @property {boolean|null} [cargoDropAuto] SaeDropDataMessage cargoDropAuto
         * @property {boolean|null} [glidersDropManual] SaeDropDataMessage glidersDropManual
         * @property {boolean|null} [glidersDropAuto] SaeDropDataMessage glidersDropAuto
         * @property {number|null} [targetLat] SaeDropDataMessage targetLat
         * @property {number|null} [targetLng] SaeDropDataMessage targetLng
         * @property {number|null} [predictedDropLat] SaeDropDataMessage predictedDropLat
         * @property {number|null} [predictedDropLng] SaeDropDataMessage predictedDropLng
         * @property {number|null} [distanceToTarget] SaeDropDataMessage distanceToTarget
         * @property {boolean|null} [cargoDropped] SaeDropDataMessage cargoDropped
         * @property {boolean|null} [glidersDropped] SaeDropDataMessage glidersDropped
         */

        /**
         * Constructs a new SaeDropDataMessage.
         * @memberof GavronTelem
         * @classdesc Represents a SaeDropDataMessage.
         * @implements ISaeDropDataMessage
         * @constructor
         * @param {GavronTelem.ISaeDropDataMessage=} [properties] Properties to set
         */
        function SaeDropDataMessage(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SaeDropDataMessage cargoDropManual.
         * @member {boolean} cargoDropManual
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.cargoDropManual = false;

        /**
         * SaeDropDataMessage cargoDropAuto.
         * @member {boolean} cargoDropAuto
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.cargoDropAuto = false;

        /**
         * SaeDropDataMessage glidersDropManual.
         * @member {boolean} glidersDropManual
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.glidersDropManual = false;

        /**
         * SaeDropDataMessage glidersDropAuto.
         * @member {boolean} glidersDropAuto
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.glidersDropAuto = false;

        /**
         * SaeDropDataMessage targetLat.
         * @member {number} targetLat
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.targetLat = 0;

        /**
         * SaeDropDataMessage targetLng.
         * @member {number} targetLng
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.targetLng = 0;

        /**
         * SaeDropDataMessage predictedDropLat.
         * @member {number} predictedDropLat
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.predictedDropLat = 0;

        /**
         * SaeDropDataMessage predictedDropLng.
         * @member {number} predictedDropLng
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.predictedDropLng = 0;

        /**
         * SaeDropDataMessage distanceToTarget.
         * @member {number} distanceToTarget
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.distanceToTarget = 0;

        /**
         * SaeDropDataMessage cargoDropped.
         * @member {boolean} cargoDropped
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.cargoDropped = false;

        /**
         * SaeDropDataMessage glidersDropped.
         * @member {boolean} glidersDropped
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         */
        SaeDropDataMessage.prototype.glidersDropped = false;

        /**
         * Creates a new SaeDropDataMessage instance using the specified properties.
         * @function create
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {GavronTelem.ISaeDropDataMessage=} [properties] Properties to set
         * @returns {GavronTelem.SaeDropDataMessage} SaeDropDataMessage instance
         */
        SaeDropDataMessage.create = function create(properties) {
            return new SaeDropDataMessage(properties);
        };

        /**
         * Encodes the specified SaeDropDataMessage message. Does not implicitly {@link GavronTelem.SaeDropDataMessage.verify|verify} messages.
         * @function encode
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {GavronTelem.ISaeDropDataMessage} message SaeDropDataMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SaeDropDataMessage.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.cargoDropManual != null && Object.hasOwnProperty.call(message, "cargoDropManual"))
                writer.uint32(/* id 1, wireType 0 =*/8).bool(message.cargoDropManual);
            if (message.cargoDropAuto != null && Object.hasOwnProperty.call(message, "cargoDropAuto"))
                writer.uint32(/* id 2, wireType 0 =*/16).bool(message.cargoDropAuto);
            if (message.glidersDropManual != null && Object.hasOwnProperty.call(message, "glidersDropManual"))
                writer.uint32(/* id 3, wireType 0 =*/24).bool(message.glidersDropManual);
            if (message.glidersDropAuto != null && Object.hasOwnProperty.call(message, "glidersDropAuto"))
                writer.uint32(/* id 4, wireType 0 =*/32).bool(message.glidersDropAuto);
            if (message.targetLat != null && Object.hasOwnProperty.call(message, "targetLat"))
                writer.uint32(/* id 5, wireType 5 =*/45).float(message.targetLat);
            if (message.targetLng != null && Object.hasOwnProperty.call(message, "targetLng"))
                writer.uint32(/* id 6, wireType 5 =*/53).float(message.targetLng);
            if (message.predictedDropLat != null && Object.hasOwnProperty.call(message, "predictedDropLat"))
                writer.uint32(/* id 7, wireType 5 =*/61).float(message.predictedDropLat);
            if (message.predictedDropLng != null && Object.hasOwnProperty.call(message, "predictedDropLng"))
                writer.uint32(/* id 8, wireType 5 =*/69).float(message.predictedDropLng);
            if (message.distanceToTarget != null && Object.hasOwnProperty.call(message, "distanceToTarget"))
                writer.uint32(/* id 9, wireType 5 =*/77).float(message.distanceToTarget);
            if (message.cargoDropped != null && Object.hasOwnProperty.call(message, "cargoDropped"))
                writer.uint32(/* id 10, wireType 0 =*/80).bool(message.cargoDropped);
            if (message.glidersDropped != null && Object.hasOwnProperty.call(message, "glidersDropped"))
                writer.uint32(/* id 11, wireType 0 =*/88).bool(message.glidersDropped);
            return writer;
        };

        /**
         * Encodes the specified SaeDropDataMessage message, length delimited. Does not implicitly {@link GavronTelem.SaeDropDataMessage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {GavronTelem.ISaeDropDataMessage} message SaeDropDataMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SaeDropDataMessage.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SaeDropDataMessage message from the specified reader or buffer.
         * @function decode
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {GavronTelem.SaeDropDataMessage} SaeDropDataMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SaeDropDataMessage.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GavronTelem.SaeDropDataMessage();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.cargoDropManual = reader.bool();
                    break;
                case 2:
                    message.cargoDropAuto = reader.bool();
                    break;
                case 3:
                    message.glidersDropManual = reader.bool();
                    break;
                case 4:
                    message.glidersDropAuto = reader.bool();
                    break;
                case 5:
                    message.targetLat = reader.float();
                    break;
                case 6:
                    message.targetLng = reader.float();
                    break;
                case 7:
                    message.predictedDropLat = reader.float();
                    break;
                case 8:
                    message.predictedDropLng = reader.float();
                    break;
                case 9:
                    message.distanceToTarget = reader.float();
                    break;
                case 10:
                    message.cargoDropped = reader.bool();
                    break;
                case 11:
                    message.glidersDropped = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SaeDropDataMessage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {GavronTelem.SaeDropDataMessage} SaeDropDataMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SaeDropDataMessage.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SaeDropDataMessage message.
         * @function verify
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SaeDropDataMessage.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.cargoDropManual != null && message.hasOwnProperty("cargoDropManual"))
                if (typeof message.cargoDropManual !== "boolean")
                    return "cargoDropManual: boolean expected";
            if (message.cargoDropAuto != null && message.hasOwnProperty("cargoDropAuto"))
                if (typeof message.cargoDropAuto !== "boolean")
                    return "cargoDropAuto: boolean expected";
            if (message.glidersDropManual != null && message.hasOwnProperty("glidersDropManual"))
                if (typeof message.glidersDropManual !== "boolean")
                    return "glidersDropManual: boolean expected";
            if (message.glidersDropAuto != null && message.hasOwnProperty("glidersDropAuto"))
                if (typeof message.glidersDropAuto !== "boolean")
                    return "glidersDropAuto: boolean expected";
            if (message.targetLat != null && message.hasOwnProperty("targetLat"))
                if (typeof message.targetLat !== "number")
                    return "targetLat: number expected";
            if (message.targetLng != null && message.hasOwnProperty("targetLng"))
                if (typeof message.targetLng !== "number")
                    return "targetLng: number expected";
            if (message.predictedDropLat != null && message.hasOwnProperty("predictedDropLat"))
                if (typeof message.predictedDropLat !== "number")
                    return "predictedDropLat: number expected";
            if (message.predictedDropLng != null && message.hasOwnProperty("predictedDropLng"))
                if (typeof message.predictedDropLng !== "number")
                    return "predictedDropLng: number expected";
            if (message.distanceToTarget != null && message.hasOwnProperty("distanceToTarget"))
                if (typeof message.distanceToTarget !== "number")
                    return "distanceToTarget: number expected";
            if (message.cargoDropped != null && message.hasOwnProperty("cargoDropped"))
                if (typeof message.cargoDropped !== "boolean")
                    return "cargoDropped: boolean expected";
            if (message.glidersDropped != null && message.hasOwnProperty("glidersDropped"))
                if (typeof message.glidersDropped !== "boolean")
                    return "glidersDropped: boolean expected";
            return null;
        };

        /**
         * Creates a SaeDropDataMessage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {GavronTelem.SaeDropDataMessage} SaeDropDataMessage
         */
        SaeDropDataMessage.fromObject = function fromObject(object) {
            if (object instanceof $root.GavronTelem.SaeDropDataMessage)
                return object;
            var message = new $root.GavronTelem.SaeDropDataMessage();
            if (object.cargoDropManual != null)
                message.cargoDropManual = Boolean(object.cargoDropManual);
            if (object.cargoDropAuto != null)
                message.cargoDropAuto = Boolean(object.cargoDropAuto);
            if (object.glidersDropManual != null)
                message.glidersDropManual = Boolean(object.glidersDropManual);
            if (object.glidersDropAuto != null)
                message.glidersDropAuto = Boolean(object.glidersDropAuto);
            if (object.targetLat != null)
                message.targetLat = Number(object.targetLat);
            if (object.targetLng != null)
                message.targetLng = Number(object.targetLng);
            if (object.predictedDropLat != null)
                message.predictedDropLat = Number(object.predictedDropLat);
            if (object.predictedDropLng != null)
                message.predictedDropLng = Number(object.predictedDropLng);
            if (object.distanceToTarget != null)
                message.distanceToTarget = Number(object.distanceToTarget);
            if (object.cargoDropped != null)
                message.cargoDropped = Boolean(object.cargoDropped);
            if (object.glidersDropped != null)
                message.glidersDropped = Boolean(object.glidersDropped);
            return message;
        };

        /**
         * Creates a plain object from a SaeDropDataMessage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof GavronTelem.SaeDropDataMessage
         * @static
         * @param {GavronTelem.SaeDropDataMessage} message SaeDropDataMessage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SaeDropDataMessage.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.cargoDropManual = false;
                object.cargoDropAuto = false;
                object.glidersDropManual = false;
                object.glidersDropAuto = false;
                object.targetLat = 0;
                object.targetLng = 0;
                object.predictedDropLat = 0;
                object.predictedDropLng = 0;
                object.distanceToTarget = 0;
                object.cargoDropped = false;
                object.glidersDropped = false;
            }
            if (message.cargoDropManual != null && message.hasOwnProperty("cargoDropManual"))
                object.cargoDropManual = message.cargoDropManual;
            if (message.cargoDropAuto != null && message.hasOwnProperty("cargoDropAuto"))
                object.cargoDropAuto = message.cargoDropAuto;
            if (message.glidersDropManual != null && message.hasOwnProperty("glidersDropManual"))
                object.glidersDropManual = message.glidersDropManual;
            if (message.glidersDropAuto != null && message.hasOwnProperty("glidersDropAuto"))
                object.glidersDropAuto = message.glidersDropAuto;
            if (message.targetLat != null && message.hasOwnProperty("targetLat"))
                object.targetLat = options.json && !isFinite(message.targetLat) ? String(message.targetLat) : message.targetLat;
            if (message.targetLng != null && message.hasOwnProperty("targetLng"))
                object.targetLng = options.json && !isFinite(message.targetLng) ? String(message.targetLng) : message.targetLng;
            if (message.predictedDropLat != null && message.hasOwnProperty("predictedDropLat"))
                object.predictedDropLat = options.json && !isFinite(message.predictedDropLat) ? String(message.predictedDropLat) : message.predictedDropLat;
            if (message.predictedDropLng != null && message.hasOwnProperty("predictedDropLng"))
                object.predictedDropLng = options.json && !isFinite(message.predictedDropLng) ? String(message.predictedDropLng) : message.predictedDropLng;
            if (message.distanceToTarget != null && message.hasOwnProperty("distanceToTarget"))
                object.distanceToTarget = options.json && !isFinite(message.distanceToTarget) ? String(message.distanceToTarget) : message.distanceToTarget;
            if (message.cargoDropped != null && message.hasOwnProperty("cargoDropped"))
                object.cargoDropped = message.cargoDropped;
            if (message.glidersDropped != null && message.hasOwnProperty("glidersDropped"))
                object.glidersDropped = message.glidersDropped;
            return object;
        };

        /**
         * Converts this SaeDropDataMessage to JSON.
         * @function toJSON
         * @memberof GavronTelem.SaeDropDataMessage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SaeDropDataMessage.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SaeDropDataMessage;
    })();

    GavronTelem.DroniadaObjectDetectionMessage = (function() {

        /**
         * Properties of a DroniadaObjectDetectionMessage.
         * @memberof GavronTelem
         * @interface IDroniadaObjectDetectionMessage
         * @property {number|null} [objectLat] DroniadaObjectDetectionMessage objectLat
         * @property {number|null} [objectLng] DroniadaObjectDetectionMessage objectLng
         * @property {number|null} [objectX] DroniadaObjectDetectionMessage objectX
         * @property {number|null} [objectY] DroniadaObjectDetectionMessage objectY
         * @property {number|null} [objectId] DroniadaObjectDetectionMessage objectId
         * @property {number|null} [objectClass] DroniadaObjectDetectionMessage objectClass
         */

        /**
         * Constructs a new DroniadaObjectDetectionMessage.
         * @memberof GavronTelem
         * @classdesc Represents a DroniadaObjectDetectionMessage.
         * @implements IDroniadaObjectDetectionMessage
         * @constructor
         * @param {GavronTelem.IDroniadaObjectDetectionMessage=} [properties] Properties to set
         */
        function DroniadaObjectDetectionMessage(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * DroniadaObjectDetectionMessage objectLat.
         * @member {number} objectLat
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         */
        DroniadaObjectDetectionMessage.prototype.objectLat = 0;

        /**
         * DroniadaObjectDetectionMessage objectLng.
         * @member {number} objectLng
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         */
        DroniadaObjectDetectionMessage.prototype.objectLng = 0;

        /**
         * DroniadaObjectDetectionMessage objectX.
         * @member {number} objectX
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         */
        DroniadaObjectDetectionMessage.prototype.objectX = 0;

        /**
         * DroniadaObjectDetectionMessage objectY.
         * @member {number} objectY
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         */
        DroniadaObjectDetectionMessage.prototype.objectY = 0;

        /**
         * DroniadaObjectDetectionMessage objectId.
         * @member {number} objectId
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         */
        DroniadaObjectDetectionMessage.prototype.objectId = 0;

        /**
         * DroniadaObjectDetectionMessage objectClass.
         * @member {number} objectClass
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         */
        DroniadaObjectDetectionMessage.prototype.objectClass = 0;

        /**
         * Creates a new DroniadaObjectDetectionMessage instance using the specified properties.
         * @function create
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {GavronTelem.IDroniadaObjectDetectionMessage=} [properties] Properties to set
         * @returns {GavronTelem.DroniadaObjectDetectionMessage} DroniadaObjectDetectionMessage instance
         */
        DroniadaObjectDetectionMessage.create = function create(properties) {
            return new DroniadaObjectDetectionMessage(properties);
        };

        /**
         * Encodes the specified DroniadaObjectDetectionMessage message. Does not implicitly {@link GavronTelem.DroniadaObjectDetectionMessage.verify|verify} messages.
         * @function encode
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {GavronTelem.IDroniadaObjectDetectionMessage} message DroniadaObjectDetectionMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DroniadaObjectDetectionMessage.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.objectLat != null && Object.hasOwnProperty.call(message, "objectLat"))
                writer.uint32(/* id 1, wireType 5 =*/13).float(message.objectLat);
            if (message.objectLng != null && Object.hasOwnProperty.call(message, "objectLng"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.objectLng);
            if (message.objectX != null && Object.hasOwnProperty.call(message, "objectX"))
                writer.uint32(/* id 3, wireType 5 =*/29).float(message.objectX);
            if (message.objectY != null && Object.hasOwnProperty.call(message, "objectY"))
                writer.uint32(/* id 4, wireType 5 =*/37).float(message.objectY);
            if (message.objectId != null && Object.hasOwnProperty.call(message, "objectId"))
                writer.uint32(/* id 5, wireType 0 =*/40).int32(message.objectId);
            if (message.objectClass != null && Object.hasOwnProperty.call(message, "objectClass"))
                writer.uint32(/* id 6, wireType 0 =*/48).int32(message.objectClass);
            return writer;
        };

        /**
         * Encodes the specified DroniadaObjectDetectionMessage message, length delimited. Does not implicitly {@link GavronTelem.DroniadaObjectDetectionMessage.verify|verify} messages.
         * @function encodeDelimited
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {GavronTelem.IDroniadaObjectDetectionMessage} message DroniadaObjectDetectionMessage message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        DroniadaObjectDetectionMessage.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a DroniadaObjectDetectionMessage message from the specified reader or buffer.
         * @function decode
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {GavronTelem.DroniadaObjectDetectionMessage} DroniadaObjectDetectionMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DroniadaObjectDetectionMessage.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GavronTelem.DroniadaObjectDetectionMessage();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.objectLat = reader.float();
                    break;
                case 2:
                    message.objectLng = reader.float();
                    break;
                case 3:
                    message.objectX = reader.float();
                    break;
                case 4:
                    message.objectY = reader.float();
                    break;
                case 5:
                    message.objectId = reader.int32();
                    break;
                case 6:
                    message.objectClass = reader.int32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a DroniadaObjectDetectionMessage message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {GavronTelem.DroniadaObjectDetectionMessage} DroniadaObjectDetectionMessage
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        DroniadaObjectDetectionMessage.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a DroniadaObjectDetectionMessage message.
         * @function verify
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        DroniadaObjectDetectionMessage.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.objectLat != null && message.hasOwnProperty("objectLat"))
                if (typeof message.objectLat !== "number")
                    return "objectLat: number expected";
            if (message.objectLng != null && message.hasOwnProperty("objectLng"))
                if (typeof message.objectLng !== "number")
                    return "objectLng: number expected";
            if (message.objectX != null && message.hasOwnProperty("objectX"))
                if (typeof message.objectX !== "number")
                    return "objectX: number expected";
            if (message.objectY != null && message.hasOwnProperty("objectY"))
                if (typeof message.objectY !== "number")
                    return "objectY: number expected";
            if (message.objectId != null && message.hasOwnProperty("objectId"))
                if (!$util.isInteger(message.objectId))
                    return "objectId: integer expected";
            if (message.objectClass != null && message.hasOwnProperty("objectClass"))
                if (!$util.isInteger(message.objectClass))
                    return "objectClass: integer expected";
            return null;
        };

        /**
         * Creates a DroniadaObjectDetectionMessage message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {GavronTelem.DroniadaObjectDetectionMessage} DroniadaObjectDetectionMessage
         */
        DroniadaObjectDetectionMessage.fromObject = function fromObject(object) {
            if (object instanceof $root.GavronTelem.DroniadaObjectDetectionMessage)
                return object;
            var message = new $root.GavronTelem.DroniadaObjectDetectionMessage();
            if (object.objectLat != null)
                message.objectLat = Number(object.objectLat);
            if (object.objectLng != null)
                message.objectLng = Number(object.objectLng);
            if (object.objectX != null)
                message.objectX = Number(object.objectX);
            if (object.objectY != null)
                message.objectY = Number(object.objectY);
            if (object.objectId != null)
                message.objectId = object.objectId | 0;
            if (object.objectClass != null)
                message.objectClass = object.objectClass | 0;
            return message;
        };

        /**
         * Creates a plain object from a DroniadaObjectDetectionMessage message. Also converts values to other types if specified.
         * @function toObject
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @static
         * @param {GavronTelem.DroniadaObjectDetectionMessage} message DroniadaObjectDetectionMessage
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        DroniadaObjectDetectionMessage.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.objectLat = 0;
                object.objectLng = 0;
                object.objectX = 0;
                object.objectY = 0;
                object.objectId = 0;
                object.objectClass = 0;
            }
            if (message.objectLat != null && message.hasOwnProperty("objectLat"))
                object.objectLat = options.json && !isFinite(message.objectLat) ? String(message.objectLat) : message.objectLat;
            if (message.objectLng != null && message.hasOwnProperty("objectLng"))
                object.objectLng = options.json && !isFinite(message.objectLng) ? String(message.objectLng) : message.objectLng;
            if (message.objectX != null && message.hasOwnProperty("objectX"))
                object.objectX = options.json && !isFinite(message.objectX) ? String(message.objectX) : message.objectX;
            if (message.objectY != null && message.hasOwnProperty("objectY"))
                object.objectY = options.json && !isFinite(message.objectY) ? String(message.objectY) : message.objectY;
            if (message.objectId != null && message.hasOwnProperty("objectId"))
                object.objectId = message.objectId;
            if (message.objectClass != null && message.hasOwnProperty("objectClass"))
                object.objectClass = message.objectClass;
            return object;
        };

        /**
         * Converts this DroniadaObjectDetectionMessage to JSON.
         * @function toJSON
         * @memberof GavronTelem.DroniadaObjectDetectionMessage
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        DroniadaObjectDetectionMessage.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return DroniadaObjectDetectionMessage;
    })();

    GavronTelem.GavronTelemFrame = (function() {

        /**
         * Properties of a GavronTelemFrame.
         * @memberof GavronTelem
         * @interface IGavronTelemFrame
         * @property {GavronTelem.IGavronTelemFrameHeader|null} [header] GavronTelemFrame header
         * @property {GavronTelem.IDroneMonitoringMessage|null} [droneMonitoringMessage] GavronTelemFrame droneMonitoringMessage
         * @property {GavronTelem.ISaeDropDataMessage|null} [saeDropDataMessage] GavronTelemFrame saeDropDataMessage
         * @property {GavronTelem.IDroniadaObjectDetectionMessage|null} [droniadaObjectDetectionMessage] GavronTelemFrame droniadaObjectDetectionMessage
         */

        /**
         * Constructs a new GavronTelemFrame.
         * @memberof GavronTelem
         * @classdesc Represents a GavronTelemFrame.
         * @implements IGavronTelemFrame
         * @constructor
         * @param {GavronTelem.IGavronTelemFrame=} [properties] Properties to set
         */
        function GavronTelemFrame(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * GavronTelemFrame header.
         * @member {GavronTelem.IGavronTelemFrameHeader|null|undefined} header
         * @memberof GavronTelem.GavronTelemFrame
         * @instance
         */
        GavronTelemFrame.prototype.header = null;

        /**
         * GavronTelemFrame droneMonitoringMessage.
         * @member {GavronTelem.IDroneMonitoringMessage|null|undefined} droneMonitoringMessage
         * @memberof GavronTelem.GavronTelemFrame
         * @instance
         */
        GavronTelemFrame.prototype.droneMonitoringMessage = null;

        /**
         * GavronTelemFrame saeDropDataMessage.
         * @member {GavronTelem.ISaeDropDataMessage|null|undefined} saeDropDataMessage
         * @memberof GavronTelem.GavronTelemFrame
         * @instance
         */
        GavronTelemFrame.prototype.saeDropDataMessage = null;

        /**
         * GavronTelemFrame droniadaObjectDetectionMessage.
         * @member {GavronTelem.IDroniadaObjectDetectionMessage|null|undefined} droniadaObjectDetectionMessage
         * @memberof GavronTelem.GavronTelemFrame
         * @instance
         */
        GavronTelemFrame.prototype.droniadaObjectDetectionMessage = null;

        // OneOf field names bound to virtual getters and setters
        var $oneOfFields;

        /**
         * GavronTelemFrame messages.
         * @member {"droneMonitoringMessage"|"saeDropDataMessage"|"droniadaObjectDetectionMessage"|undefined} messages
         * @memberof GavronTelem.GavronTelemFrame
         * @instance
         */
        Object.defineProperty(GavronTelemFrame.prototype, "messages", {
            get: $util.oneOfGetter($oneOfFields = ["droneMonitoringMessage", "saeDropDataMessage", "droniadaObjectDetectionMessage"]),
            set: $util.oneOfSetter($oneOfFields)
        });

        /**
         * Creates a new GavronTelemFrame instance using the specified properties.
         * @function create
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {GavronTelem.IGavronTelemFrame=} [properties] Properties to set
         * @returns {GavronTelem.GavronTelemFrame} GavronTelemFrame instance
         */
        GavronTelemFrame.create = function create(properties) {
            return new GavronTelemFrame(properties);
        };

        /**
         * Encodes the specified GavronTelemFrame message. Does not implicitly {@link GavronTelem.GavronTelemFrame.verify|verify} messages.
         * @function encode
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {GavronTelem.IGavronTelemFrame} message GavronTelemFrame message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GavronTelemFrame.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.header != null && Object.hasOwnProperty.call(message, "header"))
                $root.GavronTelem.GavronTelemFrameHeader.encode(message.header, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            if (message.droneMonitoringMessage != null && Object.hasOwnProperty.call(message, "droneMonitoringMessage"))
                $root.GavronTelem.DroneMonitoringMessage.encode(message.droneMonitoringMessage, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
            if (message.saeDropDataMessage != null && Object.hasOwnProperty.call(message, "saeDropDataMessage"))
                $root.GavronTelem.SaeDropDataMessage.encode(message.saeDropDataMessage, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            if (message.droniadaObjectDetectionMessage != null && Object.hasOwnProperty.call(message, "droniadaObjectDetectionMessage"))
                $root.GavronTelem.DroniadaObjectDetectionMessage.encode(message.droniadaObjectDetectionMessage, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified GavronTelemFrame message, length delimited. Does not implicitly {@link GavronTelem.GavronTelemFrame.verify|verify} messages.
         * @function encodeDelimited
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {GavronTelem.IGavronTelemFrame} message GavronTelemFrame message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        GavronTelemFrame.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a GavronTelemFrame message from the specified reader or buffer.
         * @function decode
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {GavronTelem.GavronTelemFrame} GavronTelemFrame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GavronTelemFrame.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.GavronTelem.GavronTelemFrame();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.header = $root.GavronTelem.GavronTelemFrameHeader.decode(reader, reader.uint32());
                    break;
                case 2:
                    message.droneMonitoringMessage = $root.GavronTelem.DroneMonitoringMessage.decode(reader, reader.uint32());
                    break;
                case 3:
                    message.saeDropDataMessage = $root.GavronTelem.SaeDropDataMessage.decode(reader, reader.uint32());
                    break;
                case 4:
                    message.droniadaObjectDetectionMessage = $root.GavronTelem.DroniadaObjectDetectionMessage.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a GavronTelemFrame message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {GavronTelem.GavronTelemFrame} GavronTelemFrame
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        GavronTelemFrame.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a GavronTelemFrame message.
         * @function verify
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        GavronTelemFrame.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            var properties = {};
            if (message.header != null && message.hasOwnProperty("header")) {
                var error = $root.GavronTelem.GavronTelemFrameHeader.verify(message.header);
                if (error)
                    return "header." + error;
            }
            if (message.droneMonitoringMessage != null && message.hasOwnProperty("droneMonitoringMessage")) {
                properties.messages = 1;
                {
                    var error = $root.GavronTelem.DroneMonitoringMessage.verify(message.droneMonitoringMessage);
                    if (error)
                        return "droneMonitoringMessage." + error;
                }
            }
            if (message.saeDropDataMessage != null && message.hasOwnProperty("saeDropDataMessage")) {
                if (properties.messages === 1)
                    return "messages: multiple values";
                properties.messages = 1;
                {
                    var error = $root.GavronTelem.SaeDropDataMessage.verify(message.saeDropDataMessage);
                    if (error)
                        return "saeDropDataMessage." + error;
                }
            }
            if (message.droniadaObjectDetectionMessage != null && message.hasOwnProperty("droniadaObjectDetectionMessage")) {
                if (properties.messages === 1)
                    return "messages: multiple values";
                properties.messages = 1;
                {
                    var error = $root.GavronTelem.DroniadaObjectDetectionMessage.verify(message.droniadaObjectDetectionMessage);
                    if (error)
                        return "droniadaObjectDetectionMessage." + error;
                }
            }
            return null;
        };

        /**
         * Creates a GavronTelemFrame message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {GavronTelem.GavronTelemFrame} GavronTelemFrame
         */
        GavronTelemFrame.fromObject = function fromObject(object) {
            if (object instanceof $root.GavronTelem.GavronTelemFrame)
                return object;
            var message = new $root.GavronTelem.GavronTelemFrame();
            if (object.header != null) {
                if (typeof object.header !== "object")
                    throw TypeError(".GavronTelem.GavronTelemFrame.header: object expected");
                message.header = $root.GavronTelem.GavronTelemFrameHeader.fromObject(object.header);
            }
            if (object.droneMonitoringMessage != null) {
                if (typeof object.droneMonitoringMessage !== "object")
                    throw TypeError(".GavronTelem.GavronTelemFrame.droneMonitoringMessage: object expected");
                message.droneMonitoringMessage = $root.GavronTelem.DroneMonitoringMessage.fromObject(object.droneMonitoringMessage);
            }
            if (object.saeDropDataMessage != null) {
                if (typeof object.saeDropDataMessage !== "object")
                    throw TypeError(".GavronTelem.GavronTelemFrame.saeDropDataMessage: object expected");
                message.saeDropDataMessage = $root.GavronTelem.SaeDropDataMessage.fromObject(object.saeDropDataMessage);
            }
            if (object.droniadaObjectDetectionMessage != null) {
                if (typeof object.droniadaObjectDetectionMessage !== "object")
                    throw TypeError(".GavronTelem.GavronTelemFrame.droniadaObjectDetectionMessage: object expected");
                message.droniadaObjectDetectionMessage = $root.GavronTelem.DroniadaObjectDetectionMessage.fromObject(object.droniadaObjectDetectionMessage);
            }
            return message;
        };

        /**
         * Creates a plain object from a GavronTelemFrame message. Also converts values to other types if specified.
         * @function toObject
         * @memberof GavronTelem.GavronTelemFrame
         * @static
         * @param {GavronTelem.GavronTelemFrame} message GavronTelemFrame
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        GavronTelemFrame.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.header = null;
            if (message.header != null && message.hasOwnProperty("header"))
                object.header = $root.GavronTelem.GavronTelemFrameHeader.toObject(message.header, options);
            if (message.droneMonitoringMessage != null && message.hasOwnProperty("droneMonitoringMessage")) {
                object.droneMonitoringMessage = $root.GavronTelem.DroneMonitoringMessage.toObject(message.droneMonitoringMessage, options);
                if (options.oneofs)
                    object.messages = "droneMonitoringMessage";
            }
            if (message.saeDropDataMessage != null && message.hasOwnProperty("saeDropDataMessage")) {
                object.saeDropDataMessage = $root.GavronTelem.SaeDropDataMessage.toObject(message.saeDropDataMessage, options);
                if (options.oneofs)
                    object.messages = "saeDropDataMessage";
            }
            if (message.droniadaObjectDetectionMessage != null && message.hasOwnProperty("droniadaObjectDetectionMessage")) {
                object.droniadaObjectDetectionMessage = $root.GavronTelem.DroniadaObjectDetectionMessage.toObject(message.droniadaObjectDetectionMessage, options);
                if (options.oneofs)
                    object.messages = "droniadaObjectDetectionMessage";
            }
            return object;
        };

        /**
         * Converts this GavronTelemFrame to JSON.
         * @function toJSON
         * @memberof GavronTelem.GavronTelemFrame
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        GavronTelemFrame.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return GavronTelemFrame;
    })();

    return GavronTelem;
})();

module.exports = $root;
