#!/bin/bash

echo Size of message: "$(wc -c /tmp/msg.bin)"
 
node_output="$(node test.js)"
expected_output='{
  "header": {
    "timestamp": 123123,
    "machineId": 1,
    "flightId": 5,
    "position": {
      "lat": 5,
      "lng": 10,
      "alt": 15,
      "heading": 20
    }
  }
}'

echo Expected output:
echo "$expected_output"
echo
echo Got output:
echo "$node_output"

[ "$node_output" = "$expected_output" ] 